import os
import shutil
import numpy as np

dataset_dir = "../data/pixfood20"
to_path = "../test/input/images"

# np.random.seed(2363418138)

now_seed = str(np.random.get_state()[1][0])
print(now_seed)

if os.path.exists(to_path):
    shutil.rmtree(to_path)
    os.makedirs(to_path)
else:
    os.makedirs(to_path)

files = os.listdir(dataset_dir)
for f in files:
    if np.random.rand(1) < 0.001:
        ori_path = dataset_dir + '/'+ f
        mv_path = to_path + '/'+ f
        print("%s ---> %s" % (ori_path, mv_path))
        shutil.copyfile(ori_path, mv_path)