import os
import shutil

dataset_dir = "uecfood256"
to_path = "../data/%s" % dataset_dir

if not os.path.exists(to_path):
    os.makedirs(to_path)


i = 0
for x in os.walk(dataset_dir):
    path = x[0]
    files = x[2]
    
    if path.find("ipynb_") == -1:
        for f in files:
            if f.find(".jpg") > 0:
                ori_path = "%s/%s" % (path, f)
                mv_path = "%s/%s.jpg" % (to_path, str(i).zfill(8))
                print("%s ---> %s" % (ori_path, mv_path))
                os.rename(ori_path, mv_path)
                i += 1
                
shutil.rmtree(dataset_dir)
