from keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.client import device_lib
import shutil
import os

import numpy as np
import tensorflow as tf
import cv2
import tqdm
from network import Network

IMAGE_SIZE = 256
LOCAL_SIZE = 128
HOLE_MIN = 48
HOLE_MAX = 96
LEARNING_RATE = 1e-3
BATCH_SIZE = 19
PRETRAIN_EPOCH = 25
SPLIT_RATIO = 0.1
IMAGE_PATH='data'


def get_available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']

def rm_ipynb_checkpoints(mypath):
    for x in os.walk(mypath):
        full_path = x[0]
        if full_path.find("ipynb_checkpoints") is not -1:
            shutil.rmtree(full_path)
            
def average_gradients(tower_grads):
    """Calculate the average gradient for each shared variable across all towers.
    
    Note that this function provides a synchronization point across all towers.
    
    Args:
        tower_grads: List of lists of (gradient, variable) tuples. The outer list
            is over individual gradients. The inner list is over the gradient
            calculation for each tower.
    Returns:
        List of pairs of (gradient, variable) where the gradient has been averaged
        across all towers.
    """
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims(g, 0)

            # Append on a 'tower' dimension which we will average over below.
            grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(axis=0, values=grads)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)
    return average_grads

def train():
    x = tf.placeholder(tf.float32, [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 3])
    mask = tf.placeholder(tf.float32, [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 1])
    local_x = tf.placeholder(tf.float32, [BATCH_SIZE, LOCAL_SIZE, LOCAL_SIZE, 3])
    global_completion = tf.placeholder(tf.float32, [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 3])
    local_completion = tf.placeholder(tf.float32, [BATCH_SIZE, LOCAL_SIZE, LOCAL_SIZE, 3])
    is_training = tf.placeholder(tf.bool, [])

    model = Network(x, mask, local_x, global_completion, local_completion, is_training, batch_size=BATCH_SIZE)
    sess = tf.Session()
    global_step = tf.Variable(0, name='global_step', trainable=False)
    epoch = tf.Variable(0, name='epoch', trainable=False)

    opt = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)
    
    g_train_tower_grads = []
    for gpu_name in get_available_gpus():
        with tf.device(gpu_name):
            g_train_grads = opt.compute_gradients(model.g_loss, var_list=model.g_variables)
            g_train_tower_grads.append(g_train_grads)
    g_train_grads = average_gradients(g_train_tower_grads)
    g_train_op = opt.apply_gradients(g_train_grads, global_step=global_step)
    
    # g_train_grads_and_vars = opt.compute_gradients(model.g_loss, var_list=model.g_variables)
    # g_train_op = opt.apply_gradients(g_train_grads_and_vars, global_step=global_step)
    
    d_train_tower_grads = []
    for gpu_name in get_available_gpus():
        with tf.device(gpu_name):
            d_train_grads = opt.compute_gradients(model.d_loss, var_list=model.d_variables)
            d_train_tower_grads.append(d_train_grads)
    d_train_grads = average_gradients(d_train_tower_grads)
    d_train_op = opt.apply_gradients(d_train_grads, global_step=global_step)
    
    # d_train_grads_and_vars = opt.compute_gradients(model.d_loss, var_list=model.d_variables)
    # d_train_op = opt.apply_gradients(d_train_grads_and_vars, global_step=global_step)
    
    # g_train_op = opt.minimize(model.g_loss, global_step=global_step, var_list=model.g_variables)
    # d_train_op = opt.minimize(model.d_loss, global_step=global_step, var_list=model.d_variables)

    init_op = tf.global_variables_initializer()
    sess.run(init_op)

    if tf.train.get_checkpoint_state('./backup'):
        saver = tf.train.Saver()
        saver.restore(sess, './backup/latest')
        
    rm_ipynb_checkpoints(IMAGE_PATH)
    
    data_gen = ImageDataGenerator(
        rescale=1./255,
        # rotation_range=20,
        # width_shift_range=0.2,
        # height_shift_range=0.2,
        # shear_range=0.2,
        # zoom_range=0.2,
        horizontal_flip=True,
        validation_split=SPLIT_RATIO,
    )

    train_generator = data_gen.flow_from_directory(
        IMAGE_PATH,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        class_mode=None,
        subset="training",
    )

    test_generator = data_gen.flow_from_directory(
        IMAGE_PATH,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        class_mode=None,
        subset="validation",
    )

    step_num = int(len(train_generator.filenames) / BATCH_SIZE)

    while True:
        sess.run(tf.assign(epoch, tf.add(epoch, 1)))
        print('epoch: {}'.format(sess.run(epoch)))

        # Completion 
        if sess.run(epoch) <= PRETRAIN_EPOCH:
            g_loss_value = 0
            for i in tqdm.tqdm(range(step_num)):
                x_batch = train_generator.next()
                if x_batch.shape[0] < BATCH_SIZE:
                    x_batch = train_generator.next()
                    
                points_batch, mask_batch = get_points()

                _, g_loss = sess.run([g_train_op, model.g_loss], feed_dict={x: x_batch, mask: mask_batch, is_training: True})
                g_loss_value += g_loss

            print('Completion loss: {}'.format(g_loss_value))
            
            x_batch = test_generator.next()
            if x_batch.shape[0] < BATCH_SIZE:
                    x_batch = test_generator.next()
                    
            completion = sess.run(model.completion, feed_dict={x: x_batch, mask: mask_batch, is_training: False})
            sample = np.array(completion[0] * 255, dtype=np.uint8)
            cv2.imwrite('./output/{}.jpg'.format("{0:06d}".format(sess.run(epoch))), cv2.cvtColor(sample, cv2.COLOR_RGB2BGR))


            saver = tf.train.Saver()
            saver.save(sess, './backup/latest', write_meta_graph=False)
            if sess.run(epoch) == PRETRAIN_EPOCH:
                saver.save(sess, './backup/pretrained', write_meta_graph=False)


        # Discrimitation
        else:
            g_loss_value = 0
            d_loss_value = 0
            for i in tqdm.tqdm(range(step_num)):
                x_batch = train_generator.next()
                if x_batch.shape[0] < BATCH_SIZE:
                    x_batch = train_generator.next()
                    
                points_batch, mask_batch = get_points()

                _, g_loss, completion = sess.run([g_train_op, model.g_loss, model.completion], feed_dict={x: x_batch, mask: mask_batch, is_training: True})
                g_loss_value += g_loss

                local_x_batch = []
                local_completion_batch = []
                for i in range(BATCH_SIZE):
                    x1, y1, x2, y2 = points_batch[i]
                    local_x_batch.append(x_batch[i][y1:y2, x1:x2, :])
                    local_completion_batch.append(completion[i][y1:y2, x1:x2, :])
                local_x_batch = np.array(local_x_batch)
                local_completion_batch = np.array(local_completion_batch)

                _, d_loss = sess.run(
                    [d_train_op, model.d_loss], 
                    feed_dict={x: x_batch, mask: mask_batch, local_x: local_x_batch, global_completion: completion, local_completion: local_completion_batch, is_training: True})
                d_loss_value += d_loss

            print('Completion loss: {}'.format(g_loss_value))
            print('Discriminator loss: {}'.format(d_loss_value))

            x_batch = test_generator.next()
            if x_batch.shape[0] < BATCH_SIZE:
                    x_batch = test_generator.next()
                    
            completion = sess.run(model.completion, feed_dict={x: x_batch, mask: mask_batch, is_training: False})
            sample = np.array(completion[0] * 255, dtype=np.uint8)
            cv2.imwrite('./output/{}.jpg'.format("{0:06d}".format(sess.run(epoch))), cv2.cvtColor(sample, cv2.COLOR_RGB2BGR))

            saver = tf.train.Saver()
            saver.save(sess, './backup/latest', write_meta_graph=False)


def get_points():
    points = []
    mask = []
    for i in range(BATCH_SIZE):
        x1, y1 = np.random.randint(0, IMAGE_SIZE - LOCAL_SIZE + 1, 2)
        x2, y2 = np.array([x1, y1]) + LOCAL_SIZE
        points.append([x1, y1, x2, y2])

        w, h = np.random.randint(HOLE_MIN, HOLE_MAX + 1, 2)
        p1 = x1 + np.random.randint(0, LOCAL_SIZE - w)
        q1 = y1 + np.random.randint(0, LOCAL_SIZE - h)
        p2 = p1 + w
        q2 = q1 + h
        
        m = np.zeros((IMAGE_SIZE, IMAGE_SIZE, 1), dtype=np.uint8)
        m[q1:q2 + 1, p1:p2 + 1] = 1
        mask.append(m)


    return np.array(points), np.array(mask)


if __name__ == '__main__':
    train()
    