# AIA Jupyter Hub Requirements

```
pip install --upgrade pip
pip install --upgrade --no-deps keras
pip install --upgrade --no-deps keras_applications
pip install --upgrade --no-deps keras_preprocessing
```



# Usage

## I. Prepare the training data

Download [PIXFOOD20](
https://github.com/pixnet/2018-pixnet-hackathon/blob/master/opendata/pixfood20.md)

Download [Food-101](
https://www.vision.ee.ethz.ch/datasets_extra/food-101/)

Download [UEC FOOD 256](
http://foodcam.mobi/dataset256.html)

Put the images dataset into "data_prepare" directory

```
cd data_prepare
python move_pixfood20_images.py
python move_food101_images.py
python move_uecfood256_images.py
```


## II. Train the GLCIC model

```
python train.py
```
