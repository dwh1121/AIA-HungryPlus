from keras.preprocessing.image import ImageDataGenerator
import shutil

import numpy as np
import tensorflow as tf
import cv2
import tqdm
import os
# import matplotlib.pyplot as plt
import sys
sys.path.append('..')
from network import Network

IMAGE_SIZE = 256
LOCAL_SIZE = 128
HOLE_MIN = 48
HOLE_MAX = 96
BATCH_SIZE = 1
IMAGE_PATH='input'
OUTPUT_PATH='output'

def rm_ipynb_checkpoints(mypath):
    for x in os.walk(mypath):
        full_path = x[0]
        if full_path.find("ipynb_checkpoints") is not -1:
            shutil.rmtree(full_path)
               
def test():
    x = tf.placeholder(tf.float32, [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 3])
    mask = tf.placeholder(tf.float32, [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 1])
    local_x = tf.placeholder(tf.float32, [BATCH_SIZE, LOCAL_SIZE, LOCAL_SIZE, 3])
    global_completion = tf.placeholder(tf.float32, [BATCH_SIZE, IMAGE_SIZE, IMAGE_SIZE, 3])
    local_completion = tf.placeholder(tf.float32, [BATCH_SIZE, LOCAL_SIZE, LOCAL_SIZE, 3])
    is_training = tf.placeholder(tf.bool, [])

    model = Network(x, mask, local_x, global_completion, local_completion, is_training, batch_size=BATCH_SIZE)
    sess = tf.Session()
    init_op = tf.global_variables_initializer()
    sess.run(init_op)

    saver = tf.train.Saver()
    saver.restore(sess, '../backup/latest')
    
    rm_ipynb_checkpoints(IMAGE_PATH)
    
    test_gen = ImageDataGenerator(
        rescale=1./255
    )

    test_generator = test_gen.flow_from_directory(
        IMAGE_PATH,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        seed=0,
        shuffle=False,
        class_mode=None,
    )
    
    now_image = 0
    image_list = test_generator.filenames
    step_num = int(len(image_list) / BATCH_SIZE)
    
    cnt = 0
    for j in tqdm.tqdm(range(step_num)):
        x_batch = test_generator.next()
        if x_batch.shape[0] < BATCH_SIZE:
            x_batch = test_generator.next()
            
        _, mask_batch = get_points()
        completion = sess.run(model.completion, feed_dict={x: x_batch, mask: mask_batch, is_training: False})
        for i in range(BATCH_SIZE):
            cnt += 1
            raw = x_batch[i]
            raw = np.array(raw * 255., dtype=np.uint8)
            masked = raw * (1 - mask_batch[i]) + np.ones_like(raw) * mask_batch[i] * 255
            img = completion[i]
            img = np.array(img * 255., dtype=np.uint8)
            dst = '%s/%s' % (OUTPUT_PATH, image_list[now_image].split("/")[-1])
            cv2.imwrite(dst, cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
            now_image += 1
            # dst = '%s/{}.jpg'.format("{0:06d}".format(cnt)) % OUTPUT_PATH
            # output_image([['Input', masked], ['Output', img], ['Ground Truth', raw]], dst)


def get_points():
    points = []
    mask = []
    for i in range(BATCH_SIZE):
        x1, y1 = np.random.randint(0, IMAGE_SIZE - LOCAL_SIZE + 1, 2)
        x2, y2 = np.array([x1, y1]) + LOCAL_SIZE
        points.append([x1, y1, x2, y2])

        w, h = np.random.randint(HOLE_MIN, HOLE_MAX + 1, 2)
        p1 = x1 + np.random.randint(0, LOCAL_SIZE - w)
        q1 = y1 + np.random.randint(0, LOCAL_SIZE - h)
        p2 = p1 + w
        q2 = q1 + h
        
        m = np.zeros((IMAGE_SIZE, IMAGE_SIZE, 1), dtype=np.uint8)
        m[q1:q2 + 1, p1:p2 + 1] = 1
        mask.append(m)

    return np.array(points), np.array(mask)
    

# def output_image(images, dst):
#     fig = plt.figure()
#     for i, image in enumerate(images):
#         text, img = image
#         fig.add_subplot(1, 3, i + 1)
#         plt.imshow(img)
#         plt.tick_params(labelbottom='off')
#         plt.tick_params(labelleft='off')
#         plt.gca().get_xaxis().set_ticks_position('none')
#         plt.gca().get_yaxis().set_ticks_position('none')
#         plt.xlabel(text)
#     plt.savefig(dst)
#     plt.close()


if __name__ == '__main__':
    if os.path.exists(OUTPUT_PATH):
        shutil.rmtree(OUTPUT_PATH)
        os.makedirs(OUTPUT_PATH)
    else:
        os.makedirs(OUTPUT_PATH)
    
    test()
    
